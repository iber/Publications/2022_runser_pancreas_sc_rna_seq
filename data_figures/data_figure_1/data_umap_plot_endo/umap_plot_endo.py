import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable






def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])




def generate_umap_plot_endo(fig, fig_size_cm):
    """Generate the umap plot showing the different clusters"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the data for the umap plot
    umap_plot_data = pd.read_csv(path.join(folder_path, "endo_umap_plot_data.csv"), index_col = 0, header = 0)
    
    #Group the data by louvain clusters
    umap_plot_data_grouped = umap_plot_data.groupby("louvain")
    
    color_lst = [
                    "#1b9e77",
                    "#d95f02",
                    "#7570b3"
                ]


    #Create a new axis
    ax2 = fig.add_subplot(label = "umap_plot_endo")

    #Create a new axis
    ax2_x0 = 1.75
    ax2_y0 = 11
    
    plot_w = 6
    plot_h = 5


    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    ax2_pos_cm = np.array([ax2_x0, ax2_y0, ax2_x0+plot_w, ax2_y0+plot_h])
    ax2_pos_figure = get_plot_pos(fig_size_cm, ax2_pos_cm)
    ax2.set_position(ax2_pos_figure)   

    cluster_labels = ["A", "B", "C"]

    #Generate the plot
    for cluster_id, cluster_data in umap_plot_data_grouped:

        cell_type_name = cluster_labels[cluster_id] + ". " + cluster_data.iloc[0, 3]
        ax2.scatter(
            cluster_data["x_data"], cluster_data["y_data"], 
            s = 30, 
            marker = 'o',
            label = cell_type_name,
            color = color_lst[cluster_id]
        )


    #Set the axis labels
    ax2.set_xlabel("UMAP 1", labelpad = -1)
    ax2.set_ylabel("UMAP 2", labelpad = -1)
    ax2.tick_params(color = "white")
    #ax.set_ylim(-10, 15)
    #ax.set_xlim(-13, 17)
    ax2.set_xticklabels([])
    ax2.set_yticklabels([])




    #Add a legend
    ax2.legend(
        loc = 'lower left',
        bbox_to_anchor = (-0.07, 1),
        ncol=1,
        labelspacing = 0.4,
        columnspacing = -0.5,
        borderpad = 0,
        markerscale = 0.70,
        handletextpad=-0.4,
        frameon = False,
        #edgecolor = "white",
    )



if __name__ == "__main__":
    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
        }
    plt.rcParams.update(params) 
    
    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)
    
    generate_umap_plot_endo(fig, fig_size_cm)
  
    plt.savefig("test.svg")
    plt.show()