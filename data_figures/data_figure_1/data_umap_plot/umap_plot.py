import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable








def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])






def generate_global_umap_plot(fig, fig_size_cm):
    """Generate the umap plot showing the different clusters"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the data for the umap plot
    umap_plot_data = pd.read_csv(path.join(folder_path, "umap_plot_data.csv"), index_col = 0, header = 0)
    
    #Group the data by louvain clusters
    umap_plot_data_grouped = umap_plot_data.groupby("reordered_louvain")


    #Create a new axis
    ax1_x0 = 1.75
    ax1_y0 = 20
    
    plot_w = 6
    plot_h = 5


    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    ax1_pos_cm = np.array([ax1_x0, ax1_y0, ax1_x0+plot_w, ax1_y0+plot_h])
    ax1_pos_figure = get_plot_pos(fig_size_cm, ax1_pos_cm)

    #Create a new axis
    ax1 = fig.add_subplot(label = "umap plot global")
    ax1.set_position(ax1_pos_figure)    
   

    color_lst = [
                "#6bc4ff",
                "#1f78b4",
                "#8fe344",
                "#33a02c",
                "#fc9272",
                "#de2d26",
                "#a50f15",
                "#78593a",
                "#cab2d6",
                "#6a3d9a",
                "#edde37",
                "#ed377d"
    ]
 

    #Generate the plot
    for cluster_id, cluster_data in umap_plot_data_grouped:

        cell_type_name =  cluster_data.iloc[0, 3]  #str(cluster_id) + ". " + cluster_data.iloc[0, 3]
        ax1.scatter(
            cluster_data["x_data"], cluster_data["y_data"], 
            s = 0.3, 
            marker = 'o',
            label = cell_type_name,
            color = color_lst[cluster_id]
    
        )

    #Set the axis labels
    ax1.set_xlabel("UMAP 1", labelpad = -1)
    ax1.set_ylabel("UMAP 2", labelpad = -1)
    ax1.tick_params(color = "white")
    #ax1.set_ylim(-10, 15)
    #ax1.set_xlim(-13, 17)
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])

    #Set the position of the legend

    
    #Add a legend
    ax1.legend(
        loc = 'lower left',
        bbox_to_anchor = (-0.07, 1),
        ncol=2,
        labelspacing = 0.4,
        columnspacing = -0.5,
        borderpad = 0,
        markerscale = 7,
        handletextpad=-0.4,
        frameon = False,
        #edgecolor = "white",
    )
    




if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 


    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)
    
    generate_global_umap_plot(fig, fig_size_cm)
  
    plt.savefig("test.svg")
    plt.show()