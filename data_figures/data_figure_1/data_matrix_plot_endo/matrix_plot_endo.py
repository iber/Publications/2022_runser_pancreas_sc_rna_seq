import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable





def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])




def add_gene_group_labels(ax, nb_clusters, nb_genes, group_labels):
    """Add the gene group labels on top of the genes"""

    for (i, row) in group_labels.iterrows():
        label = row["label"]
        gene_id_start = int(row["gene_id_start"])
        gene_id_stop  = int(row["gene_id_stop"])

        add_gene_group_label(ax, label, nb_clusters, nb_genes, gene_id_start, gene_id_stop)

       


def add_gene_group_label(ax, text, nb_clusters, nb_genes, gene_group_start, gene_group_stop):
    """Add the gene group labels on top of the genes"""

    print(gene_group_start, gene_group_stop)

    x_pos = (gene_group_stop + gene_group_start) / 2.0 + 0.5
    y_pos = nb_clusters * 1.1

    x_pos /= nb_genes
    y_pos /= nb_clusters

    width = (gene_group_stop - gene_group_start) * 0.8

    ax.annotate(
            text, xy=(x_pos, y_pos), xytext=(x_pos, y_pos * 1.05), xycoords='axes fraction', 
            ha='center', va='bottom',  
         
            arrowprops=dict(
                arrowstyle='-[, widthB={}, lengthB=0.3'.format(width),
                lw=1, 
            )
        )




def generate_matrix_plot_endo(fig, fig_size_cm):
    """Generate the matrix plot showing the expression of different marker genes"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the data to generate the matrix plot
    gene_expression_df = pd.read_csv(path.join(folder_path, "endo_data_matrix_plot.csv"), header = 0, index_col=0)
    gene_expression_df = gene_expression_df.sort_index(ascending=False)


    #Get the group labels
    group_label_df = pd.read_csv(path.join(folder_path, "endo_marker_gene_labels.csv"), header = 0, index_col=0)

    nb_clusters = gene_expression_df.shape[0]
    nb_genes = gene_expression_df.shape[1]


    #Set up the dimension of the plot
    plot_w = 9.5
    plot_h = 5

    cell_w = plot_w / nb_genes
    cell_h = plot_h / nb_clusters

    ax_x0 = 9
    ax_y0 = 11
    ax_x1 = ax_x0 + plot_w
    ax_y1 = ax_y0 + plot_h

    ax3_pos_cm = np.array([ax_x0, ax_y0, ax_x1, ax_y1])
    ax3_pos_figure = get_plot_pos(fig_size_cm, ax3_pos_cm)
    ax3 = fig.add_subplot(label = "matrix_plot_endo")
    ax3.set_position(ax3_pos_figure)   


    #Create the matrix plot
    im1 = ax3.pcolor(
            gene_expression_df.to_numpy(), 
            cmap="viridis", 
            edgecolors = "grey", 
            lw = 0.1
    )

    im1.set_clim(0, 1)

    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]
    axis_y_ticks_str = ["C", "B", "A"]
    ax3.set_yticks([])
    ax3.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax3.set_yticklabels(axis_y_ticks_str, minor = True)
    ax3.set_yticklabels([])
    ax3.tick_params(axis='y', which='minor', pad = 1)
    ax3.set_ylabel("Louvain subcluster id", labelpad = 1)

    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(nb_genes))
    major_axis_x_ticks_str = gene_expression_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax3.set_xticks([])
    ax3.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax3.set_xticklabels([], minor = True)
    ax3.tick_params(which='minor', length = 3)
    ax3.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True)





    #Make the colorbar for the Umap plot of Sema6d
    cax1_pos_cm = np.array([ax_x1+0.1, ax_y0,  ax_x1+0.3, ax_y1])
    cax1_pos_fig = get_plot_pos(fig_size_cm, cax1_pos_cm)

    #Add the axis for the color bar
    cax1 = fig.add_subplot(label = "matrixplot endo colorbar")
    cax1.set_position(cax1_pos_fig)
    cax1.yaxis.tick_right()
    cax1.set_xticks([])

    gradient = np.linspace(-15, 15, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax1.imshow(gradient, cmap="viridis", aspect='auto', origin='lower')
    
    cax1_ticks_str_lst = ["0", "0.25", "0.5", "0.75", "1"]
    nb_ticks = cax1_ticks_str_lst.__len__()
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))
    cax1.set_yticks(y_ticks)
    cax1.set_yticklabels(cax1_ticks_str_lst)
    cax1.tick_params(axis='y', which='major', pad = 2)
    
    cax1.yaxis.set_label_position("right")
    cax1.set_ylabel("Normalized mean expression", rotation=90, labelpad=2)


    #Add the gene group labels on top of the matrix plot
    add_gene_group_label(ax3, fig_size_cm, cell_w, group_label_df, ax3_pos_cm)







def add_gene_group_label(ax1, fig_size_cm,  cell_w, group_labels_df, ax1_pos_cm):
    """Add the gene group labels on top of the genes"""


    #Get the width and height of ax1
    ax1_w  = ax1_pos_cm[2] - ax1_pos_cm[0]
    ax1_h  = ax1_pos_cm[3] - ax1_pos_cm[1]


    #Loop over the labels
    for (i, group_label) in group_labels_df.iterrows():

        #Find where the label starts and end
        new_ax_x0 = cell_w * (group_label["gene_id_start"])
        new_ax_x1 = cell_w * (group_label["gene_id_stop"] )

        nb_genes_spanned = group_label["gene_id_stop"] - group_label["gene_id_start"]

        #Get the axis fraction position of the text
        text_pos_cm =  np.array([new_ax_x0, ax1_h + 0.3,  new_ax_x1, ax1_h+0.4])
        text_pos_fig = get_plot_pos(np.array([ax1_w, ax1_h]), text_pos_cm)

        #Get the axis fraction position of the arrow
        arrow_pos_cm =  np.array([new_ax_x0, ax1_h + 0.2,  new_ax_x1, ax1_h+0.4])
        arrow_pos_fig = get_plot_pos(np.array([ax1_w, ax1_h]), arrow_pos_cm)

        #Get the position of the text and the label
        text_x =  text_pos_fig[2] + text_pos_fig[0] 
        text_y =  text_pos_fig[1]

        arrow_x = arrow_pos_fig[2] + arrow_pos_fig[0] 
        arrow_y = arrow_pos_fig[1]


        ax1.annotate(
                group_label["label"],
                xycoords = "axes fraction",
                xy = (arrow_x, arrow_y),
                xytext = (text_x, text_y),
                ha='center', va='bottom',   
                #rotation = 90,  
            )




if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 

    


    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)
    
    generate_matrix_plot_endo(fig, fig_size_cm)
  
    plt.savefig("test.svg")
    plt.show()