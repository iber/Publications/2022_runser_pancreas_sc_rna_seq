import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable


import data_matrix_plot.matrix_plot as matrix_plot_global
import data_umap_plot.umap_plot as umap_plot_global
import data_umap_plot_endo.umap_plot_endo as umap_plot_endo
import data_matrix_plot_endo.matrix_plot_endo as matrix_plot_endo





def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])




if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 


    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)
    
    matrix_plot_global.generate_global_matrix_plot(fig, fig_size_cm)
    umap_plot_global.generate_global_umap_plot(fig, fig_size_cm)
    umap_plot_endo.generate_umap_plot_endo(fig, fig_size_cm)
    matrix_plot_endo.generate_matrix_plot_endo(fig, fig_size_cm)

    plt.savefig("raw_figure_1.svg")
    plt.show()