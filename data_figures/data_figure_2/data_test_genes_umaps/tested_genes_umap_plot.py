from re import A
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable






def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54



def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])




   







def generate_tested_genes_umap(fig, fig_size_cm):
    """Generate the umap plot showing the different clusters"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Start by loading the umap data of the epithelial cells
    umap_plot_data_all_df = pd.read_csv(path.join(folder_path, "umap_plot_data.csv"), index_col = 0, header = 0)
    expression_level_all_df = pd.read_csv(path.join(folder_path, "tested_genes_df.csv"), index_col = 0, header = 0)



    umap_plot_data_all_df["cell_id"] =  umap_plot_data_all_df.index
    umap_plot_data_epi_df = umap_plot_data_all_df.loc[umap_plot_data_all_df["subtype"].isin(['0. Epithelial trunk','1. Epithelial tip', '2. Endocrine 1', '3. Endocrine 2'])]
    expression_level_epi_df =  expression_level_all_df.iloc[umap_plot_data_epi_df["cell_id"]]

    

    #Load the data for the endothelial cells
    umap_plot_data_end_df = pd.read_csv(path.join(folder_path, "endo_umap_plot_data.csv"), index_col = 0, header = 0)    
    expression_level_end_ar = np.genfromtxt(path.join(folder_path, "sema6d_expression_ar.csv"))
    expression_level_end_df = pd.DataFrame(data = expression_level_end_ar, columns= ["Sema6d"])

    ax1_x0 = 1.8
    ax1_y0 = 4.5
    
    plot_w = 4.5
    plot_h = 4.5


    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    ax1_pos_cm = np.array([ax1_x0, ax1_y0, ax1_x0+plot_w, ax1_y0+plot_h])
    ax1_pos_figure = get_plot_pos(fig_size_cm, ax1_pos_cm)


    #Create a new axis
    ax1 = fig.add_subplot(label = "Sema6d")
    ax1.set_position(ax1_pos_figure)    
    ax1.set_title("Sema6d")


    #Starts by making the Umap plot of the Sema6d expression
    im1 = ax1.scatter(
        umap_plot_data_end_df["x_data"], 
        umap_plot_data_end_df["y_data"], 
        c = expression_level_end_df["Sema6d"],
        s = 20,
        cmap = "viridis"
    )

    im1.set_clim(0, 250)
    ax1.tick_params(color = "white")
    #ax1.set_ylim(3, 11)
    #ax1.set_xlim(9, 20.1)
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])   
    ax1.set_title("Sema6d")
    ax1.set_ylabel("Umap 2", labelpad=-5)
    

    cbar_w = 0.2
    cbar_pad = 0.1


    #Make the colorbar for the Umap plot of Sema6d
    cax1_pos_cm = np.array([ax1_x0+plot_w+0.1, ax1_y0,  ax1_x0+plot_w+0.3, ax1_y0+plot_h])
    cax1_pos_fig = get_plot_pos(fig_size_cm, cax1_pos_cm)

    #Add the axis for the color bar
    cax1 = fig.add_subplot(label = "Sema6d colorbar")
    cax1.set_position(cax1_pos_fig)
    cax1.yaxis.tick_right()
    cax1.set_xticks([])

    gradient = np.linspace(-15, 15, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax1.imshow(gradient, cmap="viridis", aspect='auto', origin='lower')
    
    cax1_ticks_str_lst = ["0", "50", "100", "150", "200", "250"]
    nb_ticks = cax1_ticks_str_lst.__len__()
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))
    cax1.set_yticks(y_ticks)
    cax1.set_yticklabels(cax1_ticks_str_lst)
    cax1.tick_params(axis='y', which='major', pad = 1)
    
    cax1.yaxis.set_label_position("right")
    cax1.set_ylabel(r"""Read count""", rotation=90, labelpad=0)



    #Make the Umap plot for Bmp7
    ax2_x0 = 8
    ax2_y0 = ax1_y0


    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    ax2_pos_cm = np.array([ax2_x0, ax2_y0, ax2_x0+plot_w, ax2_y0+plot_h])
    ax2_pos_figure = get_plot_pos(fig_size_cm, ax2_pos_cm)
    ax2 = fig.add_subplot(label = "Bmp7")
    ax2.set_position(ax2_pos_figure)    
    ax2.set_title("Bmp7")

    ax2.tick_params(color = "white")
    ax2.set_xticklabels([])
    ax2.set_yticklabels([])   
    ax2.set_xlabel("Umap 1") 



    im2 = ax2.scatter(
        umap_plot_data_epi_df["x_data"], 
        umap_plot_data_epi_df["y_data"], 
        c = expression_level_epi_df["Bmp7"],
        s = 1,
        cmap = "viridis"
    )
    im2.set_clim(0, 400)

    #Make the colorbar for Bmp7
    cax2_pos_cm = np.array([ax2_x0+plot_w+0.1, ax2_y0,  ax2_x0+plot_w+0.3, ax2_y0+plot_h])
    cax2_pos_fig = get_plot_pos(fig_size_cm, cax2_pos_cm)

    #Add the axis for the color bar
    cax2 = fig.add_subplot(label = "Bmp7 colorbar")
    cax2.set_position(cax2_pos_fig)
    cax2.yaxis.tick_right()
    cax2.set_xticks([])

    cax2.imshow(gradient, cmap="viridis", aspect='auto', origin='lower')
    
    cax2_ticks_str_lst = ["0", "100", "200", "300", "400"]
    nb_ticks = cax2_ticks_str_lst.__len__()
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))
    
    cax2.set_yticks(y_ticks)
    cax2.set_yticklabels(cax2_ticks_str_lst)
    cax2.tick_params(axis='y', which='major', pad = 1)
    cax2.yaxis.set_label_position("right")
    cax2.set_ylabel(r"""Read count""", rotation=90, labelpad=0)
    


    #Make the Umap plot for Wnt7b
    ax3_x0 = 14.28
    ax3_y0 = ax1_y0

    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    ax3_pos_cm = np.array([ax3_x0, ax3_y0, ax3_x0+plot_w, ax3_y0+plot_h])
    ax3_pos_figure = get_plot_pos(fig_size_cm, ax3_pos_cm)
    ax3 = fig.add_subplot(label = "Wnt7b")
    ax3.set_position(ax3_pos_figure)    
    ax3.set_title("Wnt7b")

    ax3.tick_params(color = "white")
    ax3.set_xticklabels([])
    ax3.set_yticklabels([])   


    im3 = ax3.scatter(
        umap_plot_data_epi_df["x_data"], 
        umap_plot_data_epi_df["y_data"], 
        c = expression_level_epi_df["Wnt7b"],
        s = 1,
        cmap = "viridis",
    )

    im3.set_clim(0, 250)



    #Make the colorbar for Bmp7
    cax3_pos_cm = np.array([ax3_x0+plot_w+0.1, ax3_y0,  ax3_x0+plot_w+0.3, ax3_y0+plot_h])
    cax3_pos_fig = get_plot_pos(fig_size_cm, cax3_pos_cm)

    #Add the axis for the color bar
    cax3 = fig.add_subplot(label = "Wnt7 colorbar")
    cax3.set_position(cax3_pos_fig)
    cax3.yaxis.tick_right()
    cax3.set_xticks([])

    cax3.imshow(gradient, cmap="viridis", aspect='auto', origin='lower')
    
    cax3_ticks_str_lst = ["0", "50", "100", "150", "200", "250"]
    nb_ticks = cax3_ticks_str_lst.__len__()
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))
    
    cax3.set_yticks(y_ticks)
    cax3.set_yticklabels(cax3_ticks_str_lst)
    cax3.tick_params(axis='y', which='major', pad = 1)
    cax3.yaxis.set_label_position("right")
    cax3.set_ylabel(r"""Read count""", rotation=90, labelpad=0)
    




        

 


if __name__ == "__main__":
    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'cmr',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
        }
    plt.rcParams.update(params) 
    
        #20cm width, 27cm height 
    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)



    generate_tested_genes_umap(fig, fig_size_cm)


    plt.show()



