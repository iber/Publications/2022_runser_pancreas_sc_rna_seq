import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap





#----------------------------------------------------------------------------------------------------------
def add_gene_group_labels(ax, nb_clusters, nb_genes, group_labels):
    """Add the gene group labels on top of the genes"""

    for (i, row) in group_labels.iterrows():
        label = row["label"]
        gene_id_start = int(row["gene_id_start"])
        gene_id_stop  = int(row["gene_id_stop"])

        add_gene_group_label(ax, label, nb_clusters, nb_genes, gene_id_start, gene_id_stop)
#----------------------------------------------------------------------------------------------------------

       






#----------------------------------------------------------------------------------------------------------
def add_gene_group_label(ax, text, nb_clusters, nb_genes, gene_group_start, gene_group_stop):
    """Add the gene group labels on top of the genes"""

    print(gene_group_start, gene_group_stop)
    x_pos = (gene_group_stop + gene_group_start) / 2.0 + 0.5
    y_pos = nb_clusters * 1.025
    x_pos /= nb_genes
    y_pos /= nb_clusters
    width = (gene_group_stop - gene_group_start) * 0.8
    ax.annotate(
            text, xy=(x_pos, y_pos), xytext=(x_pos, y_pos * 1.05), xycoords='axes fraction', 
            ha='center', va='bottom',  
            rotation = 90,          
            arrowprops=dict(
                arrowstyle='-[, widthB={}, lengthB=0.3'.format(width),
                lw=1, 
            )
        )
#----------------------------------------------------------------------------------------------------------





#----------------------------------------------------------------------------------------------------------
def generate_ligand_matrix_plots(fig, spec):
    """Generate the matrix plot showing the fold change expression of each ligand"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])


    #Get the fold change data
    fold_change_df = pd.read_csv(
        path.join(folder_path, "fold_change_ligand_df.csv"),
        header= 0,
        index_col = 0
        )
    
    fold_change_df = fold_change_df.sort_index()


    colormap_lim_lst = [3.0, 4.0, 6.0, 6.0]


    #Loop over the ligands
    for i, (ligand, fold_change_data) in enumerate(fold_change_df.iteritems()):

        #Use to position the matrix plot
        row_id = i // 2 
        col_id = i % 2 * 6
        plot_y_ticks = not col_id

        ax1 = fig.add_subplot(spec[row_id, col_id])
      
        generate_ligand_matrix_plot(ax1, ligand, fold_change_data, True, colormap_lim_lst[i])

        





#----------------------------------------------------------------------------------------------------------
  





#----------------------------------------------------------------------------------------------------------
def generate_ligand_matrix_plot(ax, ligand, fold_change_data, plot_y_ticks, colormap_lim):
    #Get the data to generate the matrix plot
    nb_clusters = fold_change_data.shape[0]
    data = fold_change_data.to_numpy()
    data = data.reshape((data.shape[0], 1))


    im = ax.pcolor(data, cmap="coolwarm", edgecolors = "grey", lw = 0.1)
    ax.set_aspect("equal")
    

    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]


    if plot_y_ticks:
        axis_y_ticks_str = fold_change_data.index
        ax.set_yticks([])
        ax.set_yticks(minor_axis_y_ticks_int, minor = True)
        ax.set_yticklabels(axis_y_ticks_str, minor = True)
        ax.set_yticklabels([])
    else:
        ax.set_yticks([])
        ax.set_yticklabels([])
        ax.set_yticks(minor_axis_y_ticks_int, minor = True)


    #Set the x axis ticks
    major_axis_x_ticks_int = [0]
    major_axis_x_ticks_str = [ligand]
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax.set_xticks([])
    ax.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True)
    ax.tick_params(which='minor', length = 3)


    #Add a color bar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size=0.075, pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label("Expression fold change", rotation=90)
    im.set_clim(0.0, colormap_lim)
#----------------------------------------------------------------------------------------------------------





#----------------------------------------------------------------------------------------------------------
def generate_target_matrix_plots(fig, spec):
    """Generate the matrix plot showing the fold change expression of each ligand"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])


    #Get the fold change data
    expressison_fraction_df = pd.read_csv(
        path.join(folder_path, "fraction_expression_target.csv"),
        header= 0,
        index_col = 0
        )


    expressison_fraction_df = expressison_fraction_df.sort_index()

    #Save in a list which target genes is associated with which ligand genes
    ligand_target_lst = [
        ["Bmpr2","Acvr1","Bmpr1a","Bmpr1b"],    #Bmp
        ["Fzd4", "Lrp5", "Lrp6", "Adgra2"],     #Wnt7b
        ["Notch1","Notch2","Notch3","Notch4"],  #Jag2
        ["Plxna1","Plxna2","Kdr"]               #Sema6d
    ]


    colormap_lim_lst = [0.3, 0.3, 0.3, 0.3]



    #Loop over the ligands
    for i, targets in enumerate(ligand_target_lst):

        #Get the fraction of cells in each population having at least 1 read of the target
        expression_fraction_subset_df = expressison_fraction_df[targets]


        #Use to position the matrix plot
        row_id = i // 2 
        col_id = 1 + (i % 2) * 6

        ax1 = fig.add_subplot(spec[row_id, col_id: col_id+4])

        generate_target_matrix_plot(ax1, expression_fraction_subset_df, colormap_lim_lst[i])

   
  

#----------------------------------------------------------------------------------------------------------







#----------------------------------------------------------------------------------------------------------
def generate_target_matrix_plot(ax, expression_fraction_subset_df, colormap_limit):
    #Get the data to generate the matrix plot
    nb_clusters = expression_fraction_subset_df.shape[0]



    top =    cm.get_cmap('Reds', 128)
    bottom = cm.get_cmap('Blues_r', 128)



    newcolors = np.vstack([
                            bottom(np.linspace(0,    0.85, 128)),
                            top(   np.linspace(0.15, 0.9, 256)),
                    ])
    newcmp = ListedColormap(newcolors, name='OrangeBlue')



    im = ax.pcolor(
        expression_fraction_subset_df.to_numpy(), 
        cmap=newcmp, 
        edgecolors = "grey", 
        lw = 0.1
    )

    ax.set_aspect("equal")
    

    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]

    ax.set_yticks([])
    ax.set_yticklabels([])
    ax.set_yticks(minor_axis_y_ticks_int,  minor = True)

    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(expression_fraction_subset_df.shape[1]))
    major_axis_x_ticks_str = expression_fraction_subset_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax.set_xticks([])
    ax.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True)
    ax.tick_params(which='minor', left = False, length = 3)


    #Add a color bar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size= 0.075, pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label("Fraction expressing\ntarget gene", rotation=90)
    im.set_clim(0.0, colormap_limit)


#----------------------------------------------------------------------------------------------------------






#----------------------------------------------------------------------------------------------------------
def set_size(w,h, ax=None):
    """ w, h: width, height in inches """
    if not ax: ax=plt.gca()
    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    figw = float(w)/(r-l)
    figh = float(h)/(t-b)
    ax.figure.set_size_inches(figw, figh)
#----------------------------------------------------------------------------------------------------------






if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 


    #We have to generate multiple axis to correctly plot this figure

    fig = plt.figure(figsize=(8, 4), constrained_layout=True)
    spec = gridspec.GridSpec(nrows=2, ncols=12,  figure=fig)
    
    generate_ligand_matrix_plots(fig, spec)
    generate_target_matrix_plots(fig, spec)


    plt.show()