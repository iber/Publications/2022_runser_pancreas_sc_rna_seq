import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable




def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_in: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])





def get_interactomic_weights(ligand_ar):
    """The NicheNet output is formatted in a way which cannot be ploted. This functions 
    selects a subset of the data and organize it as a table to be later showed as a matrix plot."""


    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Check if the data has not been already computed and stored
    #if path.exists(path.join(folder_path, "interatomic_weights.csv")):
    #    figure_data_df = pd.read_csv(path.join(folder_path, "interatomic_weights.csv"), index_col = 0, header = 0)
    #    return figure_data_df


    #Get the data to generate the matrix plot
    interactomic_df = pd.read_csv(path.join(folder_path, "active_ligand_target_links_df.csv"), header = 0, sep = " ")


    #Sort the dataframe by weights and then split it in groups
    interactomic_df_group  = interactomic_df.groupby("ligand")
    target_lst = ["Efna1", "Cav1", "Dlc1", "Hmox1"]

  
    #For each ligand, get its 2 best targets
    for ligand in ligand_ar:

        if target_lst.__len__() == 9: break

        group = interactomic_df_group.get_group(ligand)
        sorted_group = group.sort_values(['weight'],ascending=False)


        #Among the targets of that ligand find a target that has not yet been added to the target_lst
        for i, interaction_pair in sorted_group.iterrows():

            if interaction_pair["target"] not in target_lst:
                target_lst.append(interaction_pair["target"])
                break

    target_ar = np.array(target_lst)

    #Create a dataframe to store all the weights
    figure_data_df = pd.DataFrame(
        np.zeros((ligand_ar.shape[0], ligand_ar.shape[0])), 
        dtype = float, 
        index = ligand_ar,
        columns = target_ar
    )

    #Fill the dataframe with the weights
    for ligand in ligand_ar:
    
        #Get the interactomic data of the given ligand
        ligand_group_data = interactomic_df_group.get_group(ligand)



        #Find if some of the selected best targets are affected by this ligand.
        target_to_select_id = np.isin(target_ar, ligand_group_data["target"])
        target_to_select_ar = target_ar[target_to_select_id]

        for target_to_select in target_to_select_ar:
            weight = ligand_group_data.loc[ligand_group_data["target"] == target_to_select]["weight"]
            weight = weight.to_list()[0]
            figure_data_df.loc[ligand, target_to_select] = weight




    #Save the data in a csv file
    figure_data_df.to_csv(path.join(folder_path, "interatomic_weights.csv"))
    return figure_data_df
    


 


def generate_interactomic_plot(fig, fig_size_cm):
    """Generate the matrix plot showing the expression of different marker genes"""



    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])


    #Get the ligand pearson coefficients
    pearson_df = pd.read_csv(
        path.join(folder_path, "ligand_pearsoncoeff_df.csv"),
        index_col = 0,
        header = 0,
        sep = " ",
    )


    #Sort the index based on the pearson coefficients
    pearson_df = pearson_df.sort_values("Pearson", ascending = True)

    ligand_ar = ["Wnt7b"] + pearson_df.index.to_list()[-8:]
    ligand_ar = np.array(ligand_ar)
    pearson_df = pearson_df.loc[ligand_ar, :]

    #Get the interactomic stores extracted from the NicheNet database
    interatomic_scores_df = get_interactomic_weights(ligand_ar)


    nb_ligands = interatomic_scores_df.shape[0]
    nb_targets = interatomic_scores_df.shape[1]


    #Set the size of each cell of the matrix plot (in cm)
    cell_w = 0.6
    cell_h = 0.6
    ax1_w = 0.7


    #Compute the width and height of the plot
    ax2_w = cell_w * nb_targets
    ax2_h = cell_h * nb_ligands
    x0 = 11.25
    y0 = 11.75
    y1 = y0 + ax2_h
    pad = 0.1


    ax1 = fig.add_subplot(label = "pearson epi to endo")
    ax2 = fig.add_subplot(label = "interaction epi to endo")

    ax1_pos_cm = np.array([x0, y0, x0+ax1_w, y1])
    ax2_pos_cm = np.array([x0+ax1_w+pad, y0, x0+ax1_w+pad+ax2_w, y1])


    ax1_pos_fig = get_plot_pos(fig_size_cm, ax1_pos_cm)
    ax2_pos_fig = get_plot_pos(fig_size_cm, ax2_pos_cm)


    ax1.set_position(ax1_pos_fig)    
    ax2.set_position(ax2_pos_fig)  


    ax2.set_title("Interactions from epithelial\nto endothelial cells")
    ax2.set_yticklabels([])
    

    #Plot the pearson coefficients
    im1 = ax1.pcolor(
            pearson_df.to_numpy(), 
            cmap="Oranges", 
            edgecolors = "white", 
            lw = 1,
            
    )

    im1.set_clim(0.1, .3)

    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_ligands))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]

    axis_y_ticks_str = pearson_df.index
    ax1.set_yticks([])
    ax1.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax1.set_yticklabels(axis_y_ticks_str, minor = True)
    ax1.tick_params(axis='y', which='minor', pad = 0)
    ax1.set_yticklabels([])
    #ax1.set_ylabel("Ligand genes", labelpad = 3)

    #Set the x axis ticks
    major_axis_x_ticks_int = [0]
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax1.set_xticks([])
    ax1.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax1.set_xticklabels([], rotation = 90, minor = True, va = "center")
    ax1.tick_params(which='minor', length = 3)
    ax1.tick_params(axis='x', which='minor', color = "white", pad = 20)

    ax2.set_xlabel("Endothelial target genes")
    ax1.set_ylabel("Epithelial ligand genes", labelpad = 1)




    #Plot the interactomic score matrix

    #Plot the pearson coefficients
    im2 = ax2.pcolor(
            interatomic_scores_df.to_numpy(), 
            cmap="Purples", 
            edgecolors = "grey", 
            lw = 0.1,
            
    )

    im2.set_clim(0.00, 0.006)

    ax2.set_yticks([])
    ax2.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax2.set_yticklabels([], minor = True)
    ax2.set_yticklabels([])
    ax2.tick_params(axis='x', which='minor', pad = 0)
    
    #Set the x axis ticks
    major_axis_2_x_ticks_int = list(range(interatomic_scores_df.shape[1]))
    minor_axis_2_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_2_x_ticks_int] 
    ax2.set_xticks([])
    ax2.set_xticks(minor_axis_2_x_ticks_int, minor = True)
    ax2.set_xticklabels(interatomic_scores_df.columns, rotation = 90, minor = True)
    ax2.tick_params(axis= "x", which='minor', length = 3)
    ax2.tick_params(axis='y',   which='both', bottom=False, top=False, labelbottom=False, length = 0)


    


    cbar_w = 0.2
    cbar_pad = 0.1

    #Add a color bar
    cax1_pos_cm = np.array([
                                x0+ax1_w+pad+ax2_w+cbar_pad, 
                                y0, 
                                x0+ax1_w+pad+ax2_w+cbar_w+cbar_pad, 
                                y1
                            ])
    cax1_pos_fig = get_plot_pos(fig_size_cm, cax1_pos_cm)
    
    cax1 = fig.add_subplot(label = "pearson colorbar")
    cax1.set_position(cax1_pos_fig)
    cax1.yaxis.tick_right()
    cax1.set_xticks([])

    gradient = np.linspace(-15, 15, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax1.imshow(gradient, cmap="Oranges", aspect='auto', origin='lower')
    
    nb_ticks = 3
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))
    cax1.set_yticks(y_ticks)
    cax1.set_yticklabels(["0.1", "0.2", "0.3"])
    cax1.tick_params(axis='y', which='major', pad = 1)
    
    cax1.yaxis.set_label_position("right")
    cax1.set_ylabel("Ligand activity score", rotation=90, labelpad=0)



    #Add a color bar
    cax2_pos_cm = np.array([
                                x0+ax1_w+pad+ax2_w+cbar_w+cbar_pad     +1.4, 
                                y0, 
                                x0+ax1_w+pad+ax2_w+2.0*cbar_w+ cbar_pad+1.4, 
                                y1
                            ])
    cax2_pos_fig = get_plot_pos(fig_size_cm, cax2_pos_cm)
    
    cax2 = fig.add_subplot(label = "Interaction scores colorbar")
    cax2.set_position(cax2_pos_fig)
    cax2.yaxis.tick_right()
    cax2.set_xticks([])


    cax2.imshow(gradient, cmap="Purples", aspect='auto', origin='lower')
    
    nb_ticks = 4
    y_ticks = np.linspace(0, (nb_ticks-1), nb_ticks) *  (256 / (nb_ticks-1))

    cax2.set_yticks(y_ticks)
    cax2.set_yticklabels(["0", "0.002", "0.004", "0.006"], rotation=90, va = "center")
    cax2.tick_params(axis='y', which='major', pad = 1)
    cax2.yaxis.set_label_position("right")
    cax2.set_ylabel("Regulatory potential", rotation=90, labelpad=0)







if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'cmr',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 

    
    fig = plt.figure(figsize=(6, 4))
    ax1 = fig.add_subplot(111)




    interactomic_weights_df = get_interactomic_weights()

    generate_matrix_plot(ax1, interactomic_weights_df)
    plt.show()