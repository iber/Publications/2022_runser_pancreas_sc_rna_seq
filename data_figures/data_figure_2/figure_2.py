import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap


import data_diff_expressed_genes.diff_expressed_genes as diff_expressed_genes
import data_interactomic_links.data_endothelial_to_epithelial.interactomic_output_end_to_epi as interactomic_output_end_to_epi
import data_interactomic_links.data_epithelial_to_endothelial.interactomic_output_epi_to_end as interactomic_output_epi_to_end
import data_test_genes_umaps.tested_genes_umap_plot as tested_genes_umap_plot

def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_in: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])





if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'qtm',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 


    #We have to generate multiple axis to correctly plot this figure

    #20cm width, 27cm height 
    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)

    
    diff_expressed_genes.generate_fold_change_matrix(fig, fig_size_cm)
    diff_expressed_genes.generate_zscore_matrix(fig, fig_size_cm)
    #diff_expressed_genes.generate_fraction_expression(fig, fig_size_cm)


    interactomic_output_end_to_epi.generate_interactomic_plot(fig, fig_size_cm)
    interactomic_output_epi_to_end.generate_interactomic_plot(fig, fig_size_cm)

    tested_genes_umap_plot.generate_tested_genes_umap(fig, fig_size_cm)

    plt.savefig("raw_figure_2.svg")
    #plt.show()