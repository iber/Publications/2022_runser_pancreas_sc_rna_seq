import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import path, getcwd, _exit, sep
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap





#----------------------------------------------------------------------------------------------------------
def add_gene_group_labels(ax, nb_clusters, nb_genes, group_labels):
    """Add the gene group labels on top of the genes"""

    for (i, row) in group_labels.iterrows():
        label = row["label"]
        gene_id_start = int(row["gene_id_start"])
        gene_id_stop  = int(row["gene_id_stop"])

        add_gene_group_label(ax, label, nb_clusters, nb_genes, gene_id_start, gene_id_stop)
#----------------------------------------------------------------------------------------------------------

       






#----------------------------------------------------------------------------------------------------------
def add_gene_group_label(ax, text, nb_clusters, nb_genes, gene_group_start, gene_group_stop):
    """Add the gene group labels on top of the genes"""

    print(gene_group_start, gene_group_stop)
    x_pos = (gene_group_stop + gene_group_start) / 2.0 + 0.5
    y_pos = nb_clusters * 1.025
    x_pos /= nb_genes
    y_pos /= nb_clusters
    width = (gene_group_stop - gene_group_start) * 0.8
    ax.annotate(
            text, xy=(x_pos, y_pos), xytext=(x_pos, y_pos * 1.05), xycoords='axes fraction', 
            ha='center', va='bottom',  
            rotation = 90,          
            arrowprops=dict(
                arrowstyle='-[, widthB={}, lengthB=0.3'.format(width),
                lw=1, 
            )
        )
#----------------------------------------------------------------------------------------------------------

def from_cm_to_in(cm):
    return cm / 2.54


def from_in_to_cm(inch):
    return inch * 2.54


def get_plot_pos(fig_size_cm, plot_pos_cm):
    """Take in input the position of a plot in cm and returns the figure 
    position of the plot.

    fig_size_cm: the width and height of the figure in inches
    """
    #x1 and y1 are the bottom left position of the plot
    p_x1, p_y1, p_x2, p_y2 = plot_pos_cm

    assert p_x2 > p_x1, "Error in pos"
    assert p_y2 > p_y1, "Error in pos"

    f_w, f_h = fig_size_cm

    #Get the width and height of the plot
    p_w = float(p_x2) - float(p_x1)
    p_h = float(p_y2) - float(p_y1)

    #Get the width and the height of the plot as a ratio of the figure width and height
    r_w = p_w / float(f_w)
    r_h = p_h / float(f_h)

    #Get the bottom left position as a function of the figure width and height 
    r_x1 = float(p_x1) / float(f_w)
    r_y1 = float(p_y1) / float(f_h)

    #Return the position of the plot in the figure coordinate system
    return np.array([r_x1, r_y1, r_w, r_h])







#----------------------------------------------------------------------------------------------------------
def generate_fold_change_matrix(fig, fig_size_cm):
    """Generate the matrix plot showing the fold change expression of each ligand"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the fold change data
    fold_change_df = pd.read_csv(
        path.join(folder_path, "fold_change_ligand_df_2.csv"),
        header= 0,
        index_col = 0
    )

    fold_change_df = fold_change_df.sort_index  (ascending=False)
    #fold_change_df = fold_change_df.reindex(sorted(fold_change_df.columns), axis=1)



    nb_clusters = fold_change_df.shape[0]
    nb_genes    = fold_change_df.shape[1]


    #Set the size of each cell of the matrix plot (in cm)
    cell_w = 0.65
    cell_h = 0.65

    #Compute the width and height of the plot
    plot_w = cell_w * nb_genes
    plot_h = cell_h * nb_clusters
    x0 = 3
    y0 = 24.0
    x1 = x0 + plot_w
    y1 = y0 + plot_h

    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    plot_pos_cm = np.array([x0, y0, x1, y1])
    plot_pos_figure = get_plot_pos(fig_size_cm, plot_pos_cm)


    #Create a new axis
    ax = fig.add_subplot(label = "fold-change")
    ax.set_position(plot_pos_figure)    
    ax.set_title("Ligand genes")


    im = ax.pcolor(
            np.log2(fold_change_df.to_numpy()), 
            cmap="coolwarm", 
            edgecolors = "grey", 
            lw = 0.1
    )
    im.set_clim(-3, 3)

    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]

    axis_y_ticks_str = fold_change_df.index
    ax.set_yticks([])
    ax.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax.set_yticklabels(axis_y_ticks_str, minor = True)
    ax.tick_params(axis='y', which='minor', pad = -2)
    ax.set_yticklabels([])

    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(nb_genes))
    major_axis_x_ticks_str = fold_change_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax.set_xticks([])
    ax.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True, va = "center")
    ax.tick_params(which='minor', length = 3)
    ax.tick_params(axis='x', which='minor', pad = 15)
    

    #Add a color bar
    cax = fig.add_subplot(label = "fold-change colorbar")
    cbar_pos_cm = np.array([x1+0.1, y0,  x1+0.3, y1])
    cbar_pos_figure = get_plot_pos(fig_size_cm, cbar_pos_cm)
    cax.set_position(cbar_pos_figure)
    cax.yaxis.tick_right()
    cax.set_xticks([])
    gradient = np.linspace(-3, 3, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax.imshow(gradient, cmap="coolwarm", aspect='auto', origin='lower')
    
    y_ticks = np.linspace(0, 6, 7) *  (256 / 6)
    cax.set_yticks(y_ticks)
    cax.set_yticklabels([r"""$<$-3""", '-2', '-1', ' 0', ' 1', ' 2', r"""$>$3"""])
    cax.yaxis.set_label_position("right")
    cax.set_ylabel(r"""$\log_2$ fold change""", rotation=90, labelpad = -4)


    






#---------------------------------------------------------------------------------------------------------------------    
def generate_zscore_matrix(fig, fig_size_cm):
    """Generate the matrix plot showing the fold change expression of each ligand"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the fold change data
    zscore_df = pd.read_csv(
        path.join(folder_path, "zscore_ligand_df_2.csv"),
        header= 0,
        index_col = 0
    )

    zscore_df = zscore_df.sort_index  (ascending=False)
    #zscore_df = zscore_df.reindex(sorted(zscore_df.columns), axis=1)


    nb_clusters = zscore_df.shape[0]
    nb_genes    = zscore_df.shape[1]

    #Create a new axis
    ax = fig.add_subplot(label = "z-score")

    #Set the size of each cell of the matrix plot (in cm)
    cell_w = 0.65
    cell_h = 0.65

    #Compute the width and height of the plot
    plot_w = cell_w * nb_genes
    plot_h = cell_h * nb_clusters
    x0 = 3
    y0 = 18.75
    x1 = x0 + plot_w
    y1 = y0 + plot_h

    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    plot_pos_cm = np.array([x0, y0, x1, y1])


    plot_pos_figure = get_plot_pos(fig_size_cm, plot_pos_cm)
    ax.set_position(plot_pos_figure)    

    
    im = ax.pcolor(
            zscore_df.to_numpy(), 
            cmap="coolwarm", 
            edgecolors = "grey", 
            lw = 0.1
    )


    im.set_clim(-6, 6)



    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]

    axis_y_ticks_str = zscore_df.index
    ax.set_yticks([])
    ax.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax.set_yticklabels(axis_y_ticks_str, minor = True)
    ax.set_yticklabels([])
    ax.tick_params(axis='y', which='minor', pad = -2)

    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(nb_genes))
    major_axis_x_ticks_str = zscore_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    
    ax.xaxis.tick_top()
    ax.set_xticks([])
    ax.set_xticks(minor_axis_x_ticks_int, minor = True)
    #ax.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True)
    ax.set_xticklabels([], minor = True)
    ax.tick_params(which='minor', length = 3)


    #Add a color bar
    cax = fig.add_subplot(label = "z-score colorbar")
    cbar_pos_cm = np.array([x1+0.1, y0,  x1+0.3, y1])
    cbar_pos_figure = get_plot_pos(fig_size_cm, cbar_pos_cm)
    cax.set_position(cbar_pos_figure)
    cax.yaxis.tick_right()
    cax.set_xticks([])
    gradient = np.linspace(-15, 15, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax.imshow(gradient, cmap="coolwarm", aspect='auto', origin='lower')
    
    y_ticks = np.linspace(0, 6, 7) *  (256 / 6)
    cax.set_yticks(y_ticks)
    cax.set_yticklabels([r"""$<$-6""", '-4', '-2', ' 0', ' 2', ' 4', r"""$>$6"""])
    cax.yaxis.set_label_position("right")
    cax.set_ylabel(r"""z-score""", rotation=90, labelpad=-6)



#---------------------------------------------------------------------------------------------------------------------    



def generate_fraction_expression(fig, fig_size_cm):
    """Generate the matrix plot showing the fold change expression of each ligand"""

    #Get the path to the folder where this script is located
    script_path = path.realpath(__file__)
    folder_path = sep.join(script_path.split(sep)[:-1])

    #Get the fold change data
    fraction_df = pd.read_csv(
        path.join(folder_path, "fraction_expression_df_2.csv"),
        header= 0,
        index_col = 0
    )

    fraction_df = fraction_df.sort_index  (ascending=False)
    #fraction_df = fraction_df.reindex(sorted(fraction_df.columns), axis=1)


    nb_clusters = fraction_df.shape[0]
    nb_genes    = fraction_df.shape[1]

    #Create a new axis
    ax = fig.add_subplot(label = "fraction")

    #Set the size of each cell of the matrix plot (in cm)
    cell_w = 0.675
    cell_h = 0.675

    #Compute the width and height of the plot
    plot_w = cell_w * nb_genes
    plot_h = cell_h * nb_clusters
    x0 = 12
    y0 = 24.0
    x1 = x0 + plot_w
    y1 = y0 + plot_h

    #The origin is at the bottom left of the page, [p_x1, p_y1, p_x2, p_y2]
    plot_pos_cm = np.array([x0, y0, x1, y1])


    plot_pos_figure = get_plot_pos(fig_size_cm, plot_pos_cm)
    ax.set_position(plot_pos_figure)    
    ax.set_title("Target genes")


    #Create a special colormap for this plot
    top = cm.get_cmap('Reds', 128)
    bottom = cm.get_cmap('Blues_r', 128)

    newcolors = np.vstack([
                            bottom(np.linspace(0, 1, 64)),
                            top   (np.linspace(0, 1, 256)),
                            
                        ])
    newcmp = ListedColormap(newcolors, name='OrangeBlue')
  
    
    im = ax.pcolor(
            fraction_df.to_numpy(), 
            cmap=newcmp, 
            edgecolors = "grey", 
            lw = 0.1
    )


    im.set_clim(0, 1.0)


    #Set the y axis ticks
    major_axis_y_ticks_int = list(range(nb_clusters))
    minor_axis_y_ticks_int = [major_tick + 0.5 for major_tick in major_axis_y_ticks_int]

    axis_y_ticks_str = fraction_df.index
    ax.set_yticks([])
    ax.set_yticks(minor_axis_y_ticks_int, minor = True)
    ax.set_yticklabels(axis_y_ticks_str, minor = True)
    ax.set_yticklabels([])
    ax.tick_params(axis='y', which='minor', pad = -2)

    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(nb_genes))
    major_axis_x_ticks_str = fraction_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    



    #Set the x axis ticks
    major_axis_x_ticks_int = list(range(nb_genes))
    major_axis_x_ticks_str = fraction_df.columns
    minor_axis_x_ticks_int = [major_tick + 0.5 for major_tick in major_axis_x_ticks_int] 
    ax.set_xticks([])
    ax.set_xticks(minor_axis_x_ticks_int, minor = True)
    ax.set_xticklabels(major_axis_x_ticks_str, rotation = 90, minor = True)
    ax.tick_params(which='minor', length = 3)
    ax.tick_params(axis='x', which='minor', pad = 1)




    #Add a color bar
    cax = fig.add_subplot(label = "fraction colorbar")
    cbar_pos_cm = np.array([x1+0.1, y0,  x1+0.3, y1])
    cbar_pos_figure = get_plot_pos(fig_size_cm, cbar_pos_cm)
    cax.set_position(cbar_pos_figure)
    cax.yaxis.tick_right()
    cax.set_xticks([])
    gradient = np.linspace(-15, 15, 256)
    gradient = np.vstack([gradient, gradient]).T
    cax.imshow(gradient, cmap=newcmp, aspect='auto', origin='lower')
    
    y_ticks = np.linspace(0, 4, 5) *  (256 / 4)
    cax.set_yticks(y_ticks)
    cax.set_yticklabels(["0.0", '0.25', '0.5', '0.75', "1.0"])
    cax.yaxis.set_label_position("right")
    cax.set_ylabel(r"""Fraction of population""", rotation=90, labelpad=1)






#---------------------------------------------------------------------------------------------------------------------    

if __name__ == "__main__":

    params = {'text.usetex' : True,
          'font.size' : 10,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          'text.latex.preamble' : r"""
                                \usepackage{siunitx}
                                \usepackage{amsmath}
                                \usepackage{textgreek}
                                """
          }
    plt.rcParams.update(params) 


    #We have to generate multiple axis to correctly plot this figure

    #20cm width, 27cm height 
    fig_size_cm = np.array([21.0,  29.7])
    fig_size_in = from_cm_to_in(fig_size_cm)
    fig = plt.figure(figsize=fig_size_in)
    
    generate_fold_change_matrix(fig, fig_size_cm)
    generate_zscore_matrix(fig, fig_size_cm)
    generate_fraction_expression(fig, fig_size_cm)

    plt.savefig("test.svg")
    plt.show()